# test-server
## Prerequisites
- ansible installed

## What it does
Installs docker, nginx locally and deploy 3 docker containers: `server`, `postgres`, `redis`
nginx: version specified in roles/install_nginx/vars/
docker: version specified in roles/install_docker/vars/
server: variables in roles/deploy_test_server/defaults overriden by host_vars in test inventory

Usage: `ansible-playbook -i inventories/test/hosts deploy_server.yml`  
If you need to enter password when using `sudo` command, run with `--ask-become-pass`